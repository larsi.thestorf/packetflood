export class Connection {
  public x1: number;
  public y1: number;
  public x2: number;
  public y2: number;

  constructor(x1, y1, x2, y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
  }
}
