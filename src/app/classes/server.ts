export class Server {
  public name: string;
  public x: number;
  public y: number;
  public imagePath: string;

  constructor(name, imagePath, x, y) {
    this.name = name;
    this.imagePath = imagePath;
    this.x = x;
    this.y = y;
  }
}
