import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GamescreenConnectionComponent } from './gamescreen-connection.component';

describe('GamescreenConnectionComponent', () => {
  let component: GamescreenConnectionComponent;
  let fixture: ComponentFixture<GamescreenConnectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GamescreenConnectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GamescreenConnectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
