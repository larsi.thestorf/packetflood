import {Component, HostBinding, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-gamescreen-connection',
  templateUrl: './gamescreen-connection.component.html',
  styleUrls: ['./gamescreen-connection.component.scss']
})
export class GamescreenConnectionComponent implements OnInit {

  @HostBinding('style.left.px')
  @Input('x1')
  public x1 = 0;

  @HostBinding('style.top.px')
  @Input('y1')
  public y1 = 0;

  @HostBinding('title')
  public hint = 'Fibre Optics Cable';

  @Input('x2')
  public x2 = 0;

  @Input('y2')
  public y2 = 0;

  constructor() { }

  ngOnInit(): void {
  }

  @HostBinding('style.transform')
  get calculateTransform(): string {
    const angle = Math.atan2(this.y2 - this.y1, this.x2 - this.x1);
    return 'translate(-50%,-50%) rotate(' + angle + 'rad) translate(50%,50%)';
  }
  @HostBinding('style.width.px')
  get calculateWidth(): number {
    return Math.hypot(this.x2 - this.x1, this.y2 - this.y1);
  }
}
