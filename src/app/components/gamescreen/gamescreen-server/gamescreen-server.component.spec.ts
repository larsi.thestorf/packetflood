import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GamescreenServerComponent } from './gamescreen-server.component';

describe('GamescreenServerComponent', () => {
  let component: GamescreenServerComponent;
  let fixture: ComponentFixture<GamescreenServerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GamescreenServerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GamescreenServerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
