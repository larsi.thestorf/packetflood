import {Component, HostBinding, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-gamescreen-server',
  templateUrl: './gamescreen-server.component.html',
  styleUrls: ['./gamescreen-server.component.scss']
})
export class GamescreenServerComponent implements OnInit {

  @Input('imagePath') public imagePath: string;
  @Input('name') public name: string;

  @HostBinding('style.left.px')
  @Input('x') public x;

  @HostBinding('style.top.px')
  @Input('y') public y;


  constructor() { }

  ngOnInit(): void {
  }

}
