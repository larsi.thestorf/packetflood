import { Component, OnInit } from '@angular/core';
import {Server} from '../../classes/server';
import {Connection} from '../../classes/connection';

@Component({
  selector: 'app-gamescreen',
  templateUrl: './gamescreen.component.html',
  styleUrls: ['./gamescreen.component.scss']
})
export class GamescreenComponent implements OnInit {

  servers: Server[] = [];
  connections: Connection[] = [];

  constructor() { }

  ngOnInit(): void {
    this.servers.push(new Server('Serverstation', 'assets/storage-24px.svg', 100, 500));
    this.servers.push(new Server('Serverstation Plus', 'assets/storage-24px.svg', 300, 200));

    this.connections.push(new Connection(this.servers[0].x, this.servers[0].y, this.servers[1].x, this.servers[1].y));
  }

}
