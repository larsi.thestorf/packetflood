import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from '../components/app.component';
import { GamescreenComponent } from '../components/gamescreen/gamescreen.component';
import { GamescreenServerComponent } from '../components/gamescreen/gamescreen-server/gamescreen-server.component';
import { GamescreenConnectionComponent } from '../components/gamescreen/gamescreen-connection/gamescreen-connection.component';
import { NavbarComponent } from '../components/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    GamescreenComponent,
    GamescreenServerComponent,
    GamescreenConnectionComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
